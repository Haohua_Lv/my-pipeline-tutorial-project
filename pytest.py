from selenium import webdriver
from selenium.webdriver.common.by import By 
from webdriver_manager.chrome import ChromeDriverManager

print(ChromeDriverManager().install())
browser = webdriver.Chrome(ChromeDriverManager().install())
browser.get("https://www.baidu.com/")
print(browser.find_element(By.XPATH, "//meta[@name='description']").get_attribute("content"))
browser.quit()

# print("Run python script successfully!!")
